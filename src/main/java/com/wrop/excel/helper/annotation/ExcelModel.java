package com.wrop.excel.helper.annotation;

import com.alibaba.excel.read.listener.ReadListener;
import com.wrop.excel.helper.core.read.DefaultReadListener;
import com.wrop.excel.helper.dao.DataDao;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ExcelModel {

    /**
     * 业务ID
     */
    String value();

    /**
     * 业务描述
     */
    String description() default "";

    /**
     * 数据校验类
     */
    Class<?> validateClass() default ExcelModel.class;

    /**
     * 读取监听类
     */
    Class<? extends ReadListener<?>> readListener() default DefaultReadListener.class;

    /**
     * dao
     */
    Class<? extends DataDao<?>> dao();
}
