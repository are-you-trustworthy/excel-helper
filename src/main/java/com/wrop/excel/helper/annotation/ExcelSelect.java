package com.wrop.excel.helper.annotation;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ExcelSelect {

    String exp() default "";

    boolean multi() default false;

    /**
     * exp表达式计算结果得到的集合元素是否为EnumOption类型
     */
    boolean isEnumOptionAssignable() default true;

}
