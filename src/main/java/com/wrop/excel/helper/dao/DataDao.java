package com.wrop.excel.helper.dao;

import java.util.List;

public interface DataDao<T> {

    int insert(T entity);

    void insertBatch(List<T> entityList);
}
