package com.wrop.excel.helper.core.write;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteWorkbookHolder;
import com.wrop.excel.helper.annotation.ExcelSelect;
import com.wrop.excel.helper.core.analysis.EnumOption;
import com.wrop.excel.helper.util.ReflectUtil;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import com.wrop.excel.helper.core.analysis.ExcelSelectOptionCache;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.lang.NonNull;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.*;

public class DropDownSelectHandler {

    int optionValueColumn = 0;
    int optionLabelColumn = 1;

    private boolean cacheEnabled;

    private ExcelSelectOptionCache excelSelectOptionCache;

    private final MultiDropDownSelectStrategy multiDropDownSelectStrategy;

    private final SingleDropDownSelectStrategy singleDropDownSelectStrategy;

    public DropDownSelectHandler(MultiDropDownSelectStrategy multiDropDownSelectStrategy, SingleDropDownSelectStrategy singleDropDownSelectStrategy) {
        this.multiDropDownSelectStrategy = multiDropDownSelectStrategy;
        this.singleDropDownSelectStrategy = singleDropDownSelectStrategy;
    }

    public void setCacheEnabled(boolean cacheEnabled) {
        this.cacheEnabled = cacheEnabled;
    }

    public void setExcelSelectOptionCache(ExcelSelectOptionCache excelSelectOptionCache) {
        this.excelSelectOptionCache = excelSelectOptionCache;
    }

    public void handleSelect(Field field, CellWriteHandlerContext context) {
        ExcelSelect annotation = field.getDeclaredAnnotation(ExcelSelect.class);
        if (annotation != null) {
            List<EnumOption.Option> options = evalOptions(field, annotation);
            handleOptions(field, options, context, annotation.multi());
        }
    }


    private void handleOptions(Field field, List<EnumOption.Option> options, CellWriteHandlerContext context, boolean isMulti) {

        // 定义sheet的名称
        String sheetName = field.getName() +"_options";
        writeOptionSheet(sheetName, options, context);
        Head headData = context.getHeadData();
        Integer columnIndex = headData.getColumnIndex();

        // 下拉框的起始行,结束行,起始列,结束列
        CellRangeAddressList rangeAddressList = new CellRangeAddressList(1, Short.MAX_VALUE, columnIndex, columnIndex);

        WriteSheetHolder writeSheetHolder = context.getWriteSheetHolder();

        // =sheetName!$A$1:$A$9 A1到A9行数据
        String excelColumn = getExcelColumn(optionLabelColumn);
        String refers = "="+sheetName + "!$"+excelColumn+ "$1:$"+excelColumn +"$"+ (options.size());
        // 设置下拉框
        if (isMulti) {
            multiDropDownSelectStrategy.optionSetting(refers, options, writeSheetHolder, rangeAddressList);
        } else {
            singleDropDownSelectStrategy.optionSetting(refers, options, writeSheetHolder, rangeAddressList);
        }
    }

    private void writeOptionSheet(String sheetName, List<EnumOption.Option> options, CellWriteHandlerContext context) {

        WriteWorkbookHolder writeWorkbookHolder = context.getWriteWorkbookHolder();

        //创建选项sheet
        Workbook workbook = writeWorkbookHolder.getWorkbook();
        Sheet optionSheet = workbook.createSheet(sheetName);
        Name categoryName = workbook.createName();
        categoryName.setNameName(sheetName);
        //设置隐藏
        int hiddenIndex = workbook.getSheetIndex(sheetName);
        if (!workbook.isSheetHidden(hiddenIndex)) {
            workbook.setSheetHidden(hiddenIndex, true);
        }
        //循环赋值
        for (int i = 0, length = options.size(); i < length; i++) {
            // 行数
            Row row = optionSheet.getRow(i);
            if (row == null) {
                row = optionSheet.createRow(i);
            }
            // 设置header
            row.createCell(optionValueColumn).setCellValue(options.get(i).getValue() == null?"":options.get(i).getValue().toString());
            row.createCell(optionLabelColumn).setCellValue(options.get(i).getLabel());
        }
    }

    private String getExcelColumn(int num) {
        String line = "";
        int first = num / 26;
        int second = num % 26;
        if (first > 0) {
            line = (char)('A' + first - 1) + "";
        }
        line += (char)('A' + second) + "";
        return line;
    }

    public List<EnumOption.Option> evalOptions(Field field, @NonNull ExcelSelect annotation) {
        if (cacheEnabled && excelSelectOptionCache.existsOptions(field)) {
            return excelSelectOptionCache.getFieldOptions(field);
        }
        Class<?> fieldClass = field.getType();
        String exp = annotation.exp();
        List<EnumOption.Option> options;

        if (List.class.isAssignableFrom(fieldClass)) {
            Type fieldGenericType = ReflectUtil.getFieldGenericType(field, 0);
            if (fieldGenericType instanceof Class) {
                fieldClass = (Class<?>) fieldGenericType;
            }
        } else if (fieldClass.isArray()) {
            fieldClass = fieldClass.getComponentType();
        }

        if (!StringUtils.hasLength(exp) && fieldClass.isEnum()) {
            options = getEnumOptions(fieldClass);
        } else if (StringUtils.hasLength(exp)) {
            boolean enumOptionAssignable = annotation.isEnumOptionAssignable();
            options = evalExpressionOptions(exp, enumOptionAssignable);
        } else {
            options = Collections.emptyList();
        }
        if (cacheEnabled) {
            excelSelectOptionCache.cacheFieldOptions(field, options);
        }
        return options;
    }

    private List<EnumOption.Option> getEnumOptions(Class<?> fieldClass) {

        ArrayList<EnumOption.Option> options = new ArrayList<>();

        if(EnumOption.class.isAssignableFrom(fieldClass)) {
            Object[] enumConstants = fieldClass.getEnumConstants();
            for (Object enumConstant : enumConstants) {
                EnumOption.Option option = ((EnumOption<?>)enumConstant).newOption();
                options.add(option);
            }
        }
        return options;
    }

    private List<EnumOption.Option> evalExpressionOptions(String expression, boolean enumOptionAssignable) {
        ExpressionParser parser = new SpelExpressionParser();
        Object expValue = parser.parseExpression(expression).getValue(Object.class);

        if (expValue == null) {
            return Collections.emptyList();
        }

        List<?> values;

        if (expValue instanceof List) {
            values = (List<?>)expValue;
        } else if (expValue.getClass().isArray()) {
            values = Collections.singletonList(expValue);
        } else {
            return Collections.emptyList();
        }

        // 转为Option对象
        ArrayList<EnumOption.Option> options = new ArrayList<>();
        for (Object value : values) {
            EnumOption.Option option;
            if (enumOptionAssignable) {
                option = ((EnumOption<?>) value).newOption();
            }
            else {
                option = new EnumOption.Option(value.toString(), value.toString());
            }
            options.add(option);
        }

        return options;
    }

}
