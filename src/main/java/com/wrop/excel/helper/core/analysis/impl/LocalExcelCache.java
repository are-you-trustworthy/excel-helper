package com.wrop.excel.helper.core.analysis.impl;

import com.wrop.excel.helper.core.analysis.EnumOption;
import com.wrop.excel.helper.core.analysis.ExcelRequiredFieldsCache;
import com.wrop.excel.helper.core.analysis.ExcelSelectOptionCache;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocalExcelCache implements ExcelRequiredFieldsCache, ExcelSelectOptionCache {

    private static final Map<Class<?>, List<String>> filedNameMemory = new HashMap<>(8);

    private static final Map<Field, List<EnumOption.Option>> fieldOptionMemory = new HashMap<>();

    @Override
    public void cacheFieldName(Class<?> key, List<String> fieldNameList) {
        filedNameMemory.put(key, fieldNameList);
    }

    @Override
    public List<String> getFieldName(Class<?> key) {
        return filedNameMemory.get(key);
    }

    @Override
    public boolean existsFields(Class<?> key) {
        return filedNameMemory.containsKey(key);
    }


    @Override
    public void cacheFieldOptions(Field key, List<EnumOption.Option> options) {
        fieldOptionMemory.put(key, options);
    }

    @Override
    public List<EnumOption.Option> getFieldOptions(Field key) {
        return fieldOptionMemory.get(key);
    }

    @Override
    public boolean existsOptions(Field key) {
        return fieldOptionMemory.containsKey(key);
    }
}
