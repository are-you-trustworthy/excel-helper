package com.wrop.excel.helper.core.write.impl;

import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.wrop.excel.helper.core.analysis.EnumOption;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddressList;
import com.wrop.excel.helper.core.write.MultiDropDownSelectStrategy;

import java.util.List;

/**
 * poi不支持生产多选下拉框，默认采用提示的方式让用户自己填
 */
public class DefaultMultiSelectStrategy implements MultiDropDownSelectStrategy {


    @Override
    public void optionSetting(String optionsSheetRefs, List<EnumOption.Option> options, WriteSheetHolder writeSheetHolder, CellRangeAddressList rangeAddressList) {
        Sheet sheet = writeSheetHolder.getSheet();
        DataValidationHelper helper = sheet.getDataValidationHelper();

        String[] explicitListValues = options.stream()
                .map(option -> {
                    if (option.getValue() == null) {
                        return option.getLabel();
                    } else {
                        return option.getValue().toString() + " : " + option.getLabel();
                    }
                })
                .toArray(String[]::new);
        String text = String.join(System.lineSeparator() , explicitListValues);

        DataValidationConstraint constraint = helper.createFormulaListConstraint(optionsSheetRefs);
        DataValidation dataValidation = helper.createValidation(constraint, rangeAddressList);

        dataValidation.setSuppressDropDownArrow(false);
        dataValidation.setShowPromptBox(true);
        dataValidation.setEmptyCellAllowed(false);
        dataValidation.createPromptBox("多选框,选项使用','分隔", text);

        sheet.addValidationData(dataValidation);
    }
}
