package com.wrop.excel.helper.core.analysis;

import com.wrop.excel.helper.annotation.ExcelModel;
import com.wrop.excel.helper.core.analysis.impl.DefaultRequiredFieldsCollector;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class RequiredFieldAnalyser {

    private boolean cacheEnabled;

    private ExcelRequiredFieldsCache requiredFieldsCache;

    private RequiredFieldFilterChain filterChain;

    /**
     * 调用analysisRequiredFields(modelClass)时，若modelClass不同，必须保证每次调用collector都是新的对象
     */
    private RequiredFieldsCollector collector;

    public void setRequiredFieldsCache(ExcelRequiredFieldsCache requiredFieldsCache) {
        this.requiredFieldsCache = requiredFieldsCache;
    }

    public void setCacheEnabled(boolean cacheEnabled) {
        this.cacheEnabled = cacheEnabled;
    }

    public void setFilterChain(RequiredFieldFilterChain filterChain) {
        this.filterChain = filterChain;
    }

    public void setRequiredFieldsCollector(RequiredFieldsCollector requiredFieldsCollector) {
        this.collector = requiredFieldsCollector;
    }

    public List<String> getRequiredFields(Class<?> modelClass) {
        if (cacheEnabled && contains(modelClass)) {
            return getFromCache(modelClass);
        } else {
            return analysisRequiredFields(modelClass);
        }
    }

    private boolean contains(Class<?> modelCLass) {
        return requiredFieldsCache.existsFields(modelCLass);
    }

    private List<String> getFromCache(Class<?> modelClass) {
        return requiredFieldsCache.getFieldName(modelClass);
    }

    private List<String> analysisRequiredFields(Class<?> modelClass) {

        RequiredFieldsCollector anotherCollector = collector;

        if (anotherCollector == null) {
            // 每次都须新建
            anotherCollector = new DefaultRequiredFieldsCollector();
        }

        Field[] fields = modelClass.getDeclaredFields();

        //modelClass 必填字段
        for (Field field : fields) {
            filterChain.collect(field, anotherCollector);
        }

        //validateClass 必填字段
        ExcelModel excelModel = modelClass.getDeclaredAnnotation(ExcelModel.class);
        Class<?> validateClass = excelModel.validateClass();
        if (validateClass != ExcelModel.class && validateClass != modelClass) {
            Field[] declaredFields = validateClass.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                filterChain.collect(declaredField, anotherCollector);
            }
        }

        Collection<Field> requiredFields = anotherCollector.getRequiredFields();
        List<String> requiredFieldNames = requiredFields.stream().map(Field::getName).distinct().collect(Collectors.toList());
        if (cacheEnabled) {
            requiredFieldsCache.cacheFieldName(modelClass, requiredFieldNames);
        }
        return requiredFieldNames;
    }
}
