package com.wrop.excel.helper.core.write;

import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.wrop.excel.helper.core.analysis.EnumOption;
import org.apache.poi.ss.util.CellRangeAddressList;

import java.util.List;

public interface DropDownSelectStrategy {

    /**
     * 下拉框选项设置
     * @param optionsSheetRefs 含有选项信息的sheet页的数据引用
     * @param options   选项信息
     * @param writeSheetHolder    WriteSheetHolder
     * @param rangeAddressList   下拉框范围设置
     */
    void optionSetting(String optionsSheetRefs, List<EnumOption.Option> options, WriteSheetHolder writeSheetHolder, CellRangeAddressList rangeAddressList);
}
