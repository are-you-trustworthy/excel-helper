package com.wrop.excel.helper.core.analysis;

import java.util.List;

public interface ExcelRequiredFieldsCache {

    void cacheFieldName(Class<?> key, List<String> fieldNameList);

    List<String> getFieldName(Class<?> key);

    boolean existsFields(Class<?> key);
}
