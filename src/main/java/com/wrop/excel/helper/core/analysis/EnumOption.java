package com.wrop.excel.helper.core.analysis;

import java.io.Serializable;

public interface EnumOption<T extends Serializable> {

    T getValue();

    String getLabel();

    default Option newOption() {
        return new Option(getValue(), getLabel());
    }

    class Option {

        Serializable value;

        String label;

        public Option(Serializable value, String label) {
            this.value = value;
            this.label = label;
        }

        public Serializable getValue() {
            return value;
        }

        public void setValue(Serializable value) {
            this.value = value;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }
}
