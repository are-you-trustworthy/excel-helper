package com.wrop.excel.helper.core.analysis.impl;

import com.wrop.excel.helper.core.analysis.RequiredFieldsCollector;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DefaultRequiredFieldsCollector implements RequiredFieldsCollector {

    private final Set<Field> requiredFields = new HashSet<>(8);
    
    @Override
    public void addField(Field field) {
        requiredFields.add(field);
    }

    @Override
    public Collection<Field> getRequiredFields() {
        return Collections.unmodifiableSet(requiredFields);
    }

}
