package com.wrop.excel.helper.core.read;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.wrop.excel.helper.annotation.ExcelSelect;
import com.wrop.excel.helper.core.analysis.EnumOption;
import com.wrop.excel.helper.core.exception.ValueNotExpectException;
import com.wrop.excel.helper.core.write.DropDownSelectHandler;
import com.wrop.excel.helper.util.ReflectUtil;
import com.wrop.excel.helper.util.BeanUtil;
import org.springframework.lang.NonNull;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExcelSelectConverter implements Converter<Object> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return Object.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    /**
     * 读取表格内容，转化为Java对象
     */
    @Override
    public Object convertToJavaData(ReadConverterContext<?> context) {
        String cellValue = context.getReadCellData().getStringValue();
        if (cellValue == null) {
            return null;
        }

        ExcelContentProperty contentProperty = context.getContentProperty();
        Field field = contentProperty.getField();
        ExcelSelect excelSelect = field.getDeclaredAnnotation(ExcelSelect.class);
        if (excelSelect == null) {
            return null;
        }
        // 多选的情况
        if (excelSelect.multi()) {
            return convertToMultiJavaData(cellValue, field, excelSelect);
        } else {
            return convertToSingleJavaData(cellValue, field, excelSelect);
        }
    }

    private Object convertToMultiJavaData(Object cellValue, Field field, @NonNull ExcelSelect excelSelect) {
        Class<?> fieldType = field.getType();
        // 获取options列表
        List<EnumOption.Option> options = getFieldOptions(field, excelSelect);
        String[] valueArr = cellValue.toString().split(",");
        Stream<EnumOption.Option> valueStream = options.stream().filter(option -> {
            boolean find = false;
            for (String label : valueArr) {
                if (label.equals(option.getLabel())) {
                    find = true;
                    break;
                }
            }
            return find;
        });

        // 字段类型是List
        if (List.class.isAssignableFrom(fieldType)) {
            // 获取泛型
            Type fieldGenericType = ReflectUtil.getFieldGenericType(field, 0);
            if (!(fieldGenericType instanceof Class)) {
                return null;
            }
            // 转换
            return valueStream.map(option -> convertOption(option, (Class<?>) fieldGenericType)).collect(Collectors.toList());
        }
        // 字段类型是数组
        else if (fieldType.isArray()) {
            // 获取类型
            Class<?> componentType = fieldType.getComponentType();
            // 转换
            return valueStream.map(option -> convertOption(option, componentType)).toArray(Object[]::new);
        }
        // 字符串
        else if (String.class.isAssignableFrom(fieldType)) {
            // 使用逗号拼接value
            return valueStream.map(option -> option.getValue().toString()).collect(Collectors.joining(","));
        } else {
            // 报错
            throw new ValueNotExpectException("请使用合适的类型接受多选框内容");
        }
    }

    private Object convertOption(EnumOption.Option option, Class<?> targetClass) {
        if (targetClass.isEnum() && EnumOption.class.isAssignableFrom(targetClass)) {
            Object[] enumConstants = targetClass.getEnumConstants();
            for (Object enumConstant : enumConstants) {
                if (option.getValue().toString().equals(((EnumOption<?>)enumConstant).getValue().toString())) {
                    return enumConstant;
                }
            }
            return null;
        } else {
            return BeanUtil.copy(option, targetClass);
        }
    }

    private Object convertToSingleJavaData(Object cellValue, Field field, @NonNull ExcelSelect excelSelect) {
        Class<?> fieldType = field.getType();
        // 获取options列表
        List<EnumOption.Option> options = getFieldOptions(field, excelSelect);
        // 字段类型是EnumOption
        if (EnumOption.class.isAssignableFrom(fieldType)) {
            EnumOption.Option option = null;
            for (EnumOption.Option op : options) {
                if (op.getLabel().equals(cellValue.toString())) {
                    option = op;
                    break;
                }
            }

            if (option == null) {
                throw new ValueNotExpectException("下拉框内容不符合格式");
            }
            // 如果是枚举类型，则根据value获取相应的枚举
            if (fieldType.isEnum()) {
                Object[] enumConstants = fieldType.getEnumConstants();
                String label = option.getLabel();
                for (Object enumConstant : enumConstants) {
                    if (((EnumOption<?>)enumConstant).getLabel().equals(label)) {
                        return enumConstant;
                    }
                }
            }
            // 否则创建新的对象
            else {
                return BeanUtil.copy(option, fieldType);
            }
        }

        //直接返回
        return cellValue;
    }

    /**
     * 写入选项label
     */
    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Object> context) {
        ExcelContentProperty contentProperty = context.getContentProperty();
        Field field = contentProperty.getField();
        Class<?> fieldType = field.getType();

        Object fieldValue = context.getValue();

        ExcelSelect excelSelect = field.getDeclaredAnnotation(ExcelSelect.class);
        if (excelSelect != null && excelSelect.multi()) {
            return convertListToExcelData(field, fieldValue);
        }

        if (EnumOption.class.isAssignableFrom(fieldType)) {
            EnumOption<?> option = (EnumOption<?>)fieldValue;
            return new WriteCellData<String>(option.getLabel());
        }
        return new WriteCellData<String>(fieldValue.toString());
    }

    private WriteCellData<?> convertListToExcelData(Field field, Object fieldValue) {
        Class<?> fieldType = field.getType();
        if (List.class.isAssignableFrom(fieldType)) {
            // 获取泛型
            Type fieldGenericType = ReflectUtil.getFieldGenericType(field, 0);
            if (!(fieldGenericType instanceof Class)) {
                return null;
            }
            List<?> listValue = (List<?>)fieldValue;
            String value = listValue.stream().map(v -> {
                if (v instanceof EnumOption) {
                    return ((EnumOption<?>) v).getLabel();
                } else {
                    return v.toString();
                }
            }).collect(Collectors.joining(","));
            return new WriteCellData<String>(value);
        } else if (fieldType.isArray()) {
            String value = Arrays.stream((Object[]) fieldValue).map(v -> {
                if (v instanceof EnumOption) {
                    return ((EnumOption<?>) v).getLabel();
                } else {
                    return v.toString();
                }
            }).collect(Collectors.joining(","));
            return new WriteCellData<String>(value);
        } else {
            return new WriteCellData<String>(fieldValue.toString());
        }
    }


    private List<EnumOption.Option> getFieldOptions(Field field, @NonNull ExcelSelect annotation) {
        DropDownSelectHandler dropDownSelectHandler = SpringUtil.getBean(DropDownSelectHandler.class);
        return dropDownSelectHandler.evalOptions(field, annotation);
    }
}
