package com.wrop.excel.helper.core.analysis.impl;

import com.wrop.excel.helper.annotation.Required;
import com.wrop.excel.helper.core.analysis.RequiredFieldFilter;

import java.lang.reflect.Field;

public class DefaultRequiredFieldFilter implements RequiredFieldFilter {

    @Override
    public boolean isRequired(Field field) {
        Required required = field.getDeclaredAnnotation(Required.class);
        return required != null;
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
