package com.wrop.excel.helper.core.read;

import com.wrop.excel.helper.core.exception.RepeatDataException;

public class DefaultReadListener extends AbstractExcelReadListener<Object, Object> {

    @Override
    protected void check(Object row) throws RepeatDataException {}

    @Override
    protected void fillData(Object o) {}

    @Override
    protected void validate(Object o) {
        super.validate(o);
    }

    @Override
    protected Object parseToEntity(Object o) {
        return super.parseToEntity(o);
    }

}
