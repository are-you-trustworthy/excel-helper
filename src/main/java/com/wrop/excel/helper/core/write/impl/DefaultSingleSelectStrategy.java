package com.wrop.excel.helper.core.write.impl;

import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.wrop.excel.helper.core.analysis.EnumOption;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddressList;
import com.wrop.excel.helper.core.write.SingleDropDownSelectStrategy;

import java.util.List;

public class DefaultSingleSelectStrategy implements SingleDropDownSelectStrategy {

    @Override
    public void optionSetting(String optionsSheetRefs, List<EnumOption.Option> options, WriteSheetHolder writeSheetHolder, CellRangeAddressList rangeAddressList) {
        Sheet sheet = writeSheetHolder.getSheet();
        DataValidationHelper helper = sheet.getDataValidationHelper();

        // 将sheet引用到下拉列表中
        DataValidationConstraint constraint = helper.createFormulaListConstraint(optionsSheetRefs);

        DataValidation dataValidation = helper.createValidation(constraint, rangeAddressList);
        dataValidation.setShowErrorBox(true);
        dataValidation.setErrorStyle(DataValidation.ErrorStyle.STOP);
        dataValidation.setEmptyCellAllowed(false);

        sheet.addValidationData(dataValidation);
    }
}
