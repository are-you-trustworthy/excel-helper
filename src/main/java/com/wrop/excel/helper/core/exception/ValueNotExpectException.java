package com.wrop.excel.helper.core.exception;

public class ValueNotExpectException extends RuntimeException {
    public ValueNotExpectException() {
        super();
    }

    public ValueNotExpectException(String message) {
        super(message);
    }

    public ValueNotExpectException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValueNotExpectException(Throwable cause) {
        super(cause);
    }

    protected ValueNotExpectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
