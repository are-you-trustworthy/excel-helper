package com.wrop.excel.helper.core.analysis;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public interface RequiredFieldsCollector {

    void addField(Field field);

    Collection<Field> getRequiredFields();

}
