package com.wrop.excel.helper.core.write;

import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.wrop.excel.helper.core.analysis.RequiredFieldAnalyser;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 必填字段提示处理
 */
public class RequiredFieldHeadHandler {

    private final RequiredFieldAnalyser requiredFieldAnalyser;

    public RequiredFieldHeadHandler(RequiredFieldAnalyser requiredFieldAnalyser) {
        this.requiredFieldAnalyser = requiredFieldAnalyser;
    }

    public void handleRequiredFields(Class<?> beanClass, Field currentField, WriteCellStyle writeCellStyle) {
        // modelBean的必填字段
        List<String> requiredFields = requiredFieldAnalyser.getRequiredFields(beanClass);

        String fieldName = currentField.getName();
        if (requiredFields.contains(fieldName)) {
            writeCellStyle.setFillForegroundColor(IndexedColors.RED1.getIndex());
            writeCellStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
        }
    }
}
