package com.wrop.excel.helper.core.validate;

public interface ExcelRowValidator {

    /**
     * 表格数据校验
     * @param row 一行数据
     * @param args 校验参数
     */
    Object validate(Object row, Object... args);
}
