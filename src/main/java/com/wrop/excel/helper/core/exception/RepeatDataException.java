package com.wrop.excel.helper.core.exception;

public class RepeatDataException extends RuntimeException {

    public RepeatDataException() {
        super();
    }

    public RepeatDataException(String message) {
        super(message);
    }

    public RepeatDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepeatDataException(Throwable cause) {
        super(cause);
    }

    protected RepeatDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
