package com.wrop.excel.helper.core.analysis;

import org.springframework.core.Ordered;

import java.lang.reflect.Field;

public interface RequiredFieldFilter extends Ordered {

    boolean isRequired(Field field);
}
