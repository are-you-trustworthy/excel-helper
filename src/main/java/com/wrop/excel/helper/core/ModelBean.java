package com.wrop.excel.helper.core;

import com.alibaba.excel.read.listener.ReadListener;
import com.wrop.excel.helper.dao.DataDao;

public class ModelBean {

    /**
     * 业务标识
     */
    private String bid;

    private String description;

    private String beanName;

    /**
     * 表格数据格式校验类
     */
    private Class<?> validateClass;

    /**
     * ExcelModel修饰的类
     */
    private Class<?> beanClass;

    /**
     * 数据库entity
     */
    private Class<?> entityClass;

    /**
     * dao
     */
    private Class<? extends DataDao<?>> dao;

    /**
     * 读监听器
     */
    private Class<? extends ReadListener<?>> readListener;

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public Class<?> getValidateClass() {
        return validateClass;
    }

    public void setValidateClass(Class<?> validateClass) {
        this.validateClass = validateClass;
    }

    public Class<?> getBeanClass() {
        return beanClass;
    }

    public void setBeanClass(Class<?> beanClass) {
        this.beanClass = beanClass;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    public Class<? extends ReadListener<?>> getReadListener() {
        return readListener;
    }

    public void setReadListener(Class<? extends ReadListener<?>> readListener) {
        this.readListener = readListener;
    }

    public Class<? extends DataDao<?>> getDao() {
        return dao;
    }

    public void setDao(Class<? extends DataDao<?>> dao) {
        this.dao = dao;
    }
}
