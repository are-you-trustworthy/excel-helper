package com.wrop.excel.helper.core.analysis;

import org.springframework.core.Ordered;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class RequiredFieldFilterChain {

    private final List<RequiredFieldFilter> fieldFilters = new ArrayList<>(2);

    public void addValidator(RequiredFieldFilter[] validators) {
        fieldFilters.addAll(Arrays.asList(validators));
        fieldFilters.sort(Comparator.comparingInt(Ordered::getOrder));
    }

    public void collect(Field field, RequiredFieldsCollector requiredFieldsCollector) {
        boolean required;
        for (RequiredFieldFilter fieldFilter : fieldFilters) {
            required = fieldFilter.isRequired(field);
            if (required) {
                requiredFieldsCollector.addField(field);
                break;
            }
        }
    }
}
