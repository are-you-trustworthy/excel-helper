package com.wrop.excel.helper.core.write.impl;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.util.BooleanUtils;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.wrop.excel.helper.core.write.DropDownSelectHandler;
import com.wrop.excel.helper.core.write.RequiredFieldHeadHandler;

import java.lang.reflect.Field;

public class DefaultCellWriteHandler implements CellWriteHandler {

    private DropDownSelectHandler dropDownSelectHandler;

    private RequiredFieldHeadHandler requiredFieldHeadHandler;

    public DefaultCellWriteHandler() {
    }

    public void setDropDownSelectHandler(DropDownSelectHandler dropDownSelectHandler) {
        this.dropDownSelectHandler = dropDownSelectHandler;
    }

    public void setRequiredFieldHeadHandler(RequiredFieldHeadHandler requiredFieldHeadHandler) {
        this.requiredFieldHeadHandler = requiredFieldHeadHandler;
    }

    @Override
    public void afterCellDispose(CellWriteHandlerContext context) {
        // 每个字段只处理一次
        if (BooleanUtils.isNotTrue(context.getHead())) {
            return;
        }
        Head headData = context.getHeadData();
        Field field = headData.getField();

        // 必填字段处理
        if (requiredFieldHeadHandler != null) {
            Class<?> clazz = context.getWriteSheetHolder().getClazz();
            WriteCellData<?> firstCellData = context.getFirstCellData();
            WriteCellStyle writeCellStyle = firstCellData.getOrCreateStyle();
            requiredFieldHeadHandler.handleRequiredFields(clazz, field, writeCellStyle);
        }

        // 处理下拉框
        if (dropDownSelectHandler != null) {
            dropDownSelectHandler.handleSelect(field, context);
        }


    }

}
