package com.wrop.excel.helper.core.analysis;

import java.lang.reflect.Field;
import java.util.List;

public interface ExcelSelectOptionCache {

    void cacheFieldOptions(Field key, List<EnumOption.Option> options);

    List<EnumOption.Option> getFieldOptions(Field key);

    boolean existsOptions(Field key);
}
