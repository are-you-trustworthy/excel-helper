package com.wrop.excel.helper.core.read;

import cn.hutool.json.JSONObject;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelAnalysisException;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.excel.metadata.CellExtra;
import com.wrop.excel.helper.core.ModelBean;
import com.wrop.excel.helper.dao.DataDao;
import com.wrop.excel.helper.annotation.ExcelModel;
import com.wrop.excel.helper.core.exception.RepeatDataException;
import com.wrop.excel.helper.util.BeanUtil;
import com.wrop.excel.helper.util.ExcelUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 抽象类
 * @param <E> EntityClass
 * @param <T> ModelBeanClass
 */
public abstract class AbstractExcelReadListener<E, T> extends AnalysisEventListener<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractExcelReadListener.class);

    private final boolean batchInsert;

    private DataDao<E> dao;

    private Integer BATCH_COUNT = 50;

    private ModelBean modelBean;

    private final List<E> entityList = new ArrayList<>(BATCH_COUNT);

    protected ExcelResult<T> excelResult;

    private Map<Integer, String> headMap;

    public AbstractExcelReadListener() {
        this(false);
    }

    public AbstractExcelReadListener(boolean batchInsert) {
        this.batchInsert = batchInsert;
        this.excelResult = new DefaultExcelResult<>();
    }

    public DataDao<E> getDao() {
        return dao;
    }

    public void setDao(DataDao<E> dao) {
        this.dao = dao;
    }

    public Integer getBatchCount() {
        return BATCH_COUNT;
    }

    public void setBatchCount(Integer BATCH_COUNT) {
        this.BATCH_COUNT = BATCH_COUNT;
    }

    public ModelBean getModelBean() {
        return modelBean;
    }

    public void setModelBean(ModelBean modelBean) {
        this.modelBean = modelBean;
    }

    public ExcelResult<T> getExcelResult() {
        return excelResult;
    }

    @Override
    public void invoke(T data, AnalysisContext analysisContext) {

        if (dao == null) {
            DataDao<?> dataDao = ExcelUtil.getDataDao(modelBean.getDao(), modelBean.getEntityClass());
            if (dataDao == null) {
                throw new RuntimeException("未配置dao，无法持久化");
            }
            dao = (DataDao<E>) dataDao;
        }

        deleteId(data);

        fillData(data);

        validate(data);

        check(data);

        E e;
        if (modelBean.getBeanClass() != modelBean.getEntityClass()) {
            e = parseToEntity(data);
        } else {
            e = (E)data;
        }

        if (batchInsert) {
            entityList.add(e);
            if (entityList.size() >= BATCH_COUNT || !hasNext(analysisContext)) {
                saveList();
                clear();
            }
        } else {
            saveOne(e);
        }
    }

    protected void deleteId(T t) {
        Field f = ReflectionUtils.findField(t.getClass(), "id");
        if (f != null) {
            // 删除id值
            f.setAccessible(true);
            ReflectionUtils.setField(f, t, null);
        }
    }

    /**
     * 数据库唯一性检查
     */
    protected abstract void check(T row) throws RepeatDataException;

    /**
     * 填充缺失的字段
     */
    protected abstract void fillData(T t);

    /**
     * 校验当前字段值的合法性
     */
    protected void validate(T t) {
        Class<?> validateClass = modelBean.getValidateClass();
        Class<?> modelClass = t.getClass();
        if (ExcelModel.class == validateClass || modelClass == validateClass) {
            // 默认校验@ExcelModel注解的类
            ExcelUtil.validate(t);
        } else {
            // 将行数据拷贝到校验类中，再校验数据合法性
            Object validateBean = BeanUtil.copy(t, validateClass);
            ExcelUtil.validate(validateBean);
        }
    }

    /**
     * 将数据转化为对应的entity对象
     */
    protected E parseToEntity(T t) {
        Class<?> entityClass = modelBean.getEntityClass();
        return (E)BeanUtil.copy(t, entityClass);
    }

    protected void saveOne(E e) {
        dao.insert(e);
    }

    protected void saveList() {
        dao.insertBatch(entityList);
    }

    protected void clear() {
        entityList.clear();
    }

    /**
     * 处理异常
     *
     * @param exception ExcelDataConvertException
     * @param context   Excel 上下文
     */
    @Override
    public void onException(Exception exception, AnalysisContext context) throws Exception {
        String errMsg = null;
        if (exception instanceof ExcelDataConvertException) {
            // 如果是某一个单元格的转换异常 能获取到具体行号
            ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException) exception;
            Integer rowIndex = excelDataConvertException.getRowIndex();
            Integer columnIndex = excelDataConvertException.getColumnIndex();
            errMsg = String.format("第{%d}行-第{%d}列-表头{%s}: 解析异常<br/>: {%s}",
                    rowIndex + 1, columnIndex + 1, headMap.get(columnIndex), exception.getCause().getMessage());
            if (logger.isDebugEnabled()) {
                logger.error(errMsg);
            }
            excelResult.getErrorList().add(errMsg);
            return;
        }
        else if (exception instanceof ConstraintViolationException) {
            ConstraintViolationException constraintViolationException = (ConstraintViolationException) exception;
            Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
            String constraintViolationsMsg = constraintViolations.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(","));
            errMsg = String.format("第{%d}行数据校验异常: {%s}", context.readRowHolder().getRowIndex() + 1, constraintViolationsMsg);
            if (logger.isDebugEnabled()) {
                logger.error(errMsg);
            }
            excelResult.getErrorList().add(errMsg);
            return;
        } else if (exception instanceof RepeatDataException) {
            errMsg = String.format("第{%d}行数据导入异常: {%s}", context.readRowHolder().getRowIndex() + 1, exception.getMessage());
            if (logger.isDebugEnabled()) {
                logger.error(errMsg);
            }
            excelResult.getErrorList().add(errMsg);
            return;
        }
        throw new ExcelAnalysisException(exception.getMessage(), exception);
    }

    @Override
    public void extra(CellExtra extra, AnalysisContext context) {
        logger.info("读取到了一条额外信息:{}", new JSONObject(extra));
        switch (extra.getType()) {
            case COMMENT:
                logger.info("额外信息是批注,在rowIndex:{},columnIndex;{},内容是:{}", extra.getRowIndex(), extra.getColumnIndex(),
                        extra.getText());
                break;
            case HYPERLINK:
                if ("Sheet1!A1".equals(extra.getText())) {
                    logger.info("额外信息是超链接,在rowIndex:{},columnIndex;{},内容是:{}", extra.getRowIndex(),
                            extra.getColumnIndex(), extra.getText());
                } else if ("Sheet2!A1".equals(extra.getText())) {
                    logger.info(
                            "额外信息是超链接,而且覆盖了一个区间,在firstRowIndex:{},firstColumnIndex;{},lastRowIndex:{},lastColumnIndex:{},"
                                    + "内容是:{}",
                            extra.getFirstRowIndex(), extra.getFirstColumnIndex(), extra.getLastRowIndex(),
                            extra.getLastColumnIndex(), extra.getText());
                } else {
                    logger.error("Unknown hyperlink!");
                }
                break;
            case MERGE:
                logger.info(
                        "额外信息是超链接,而且覆盖了一个区间,在firstRowIndex:{},firstColumnIndex;{},lastRowIndex:{},lastColumnIndex:{}",
                        extra.getFirstRowIndex(), extra.getFirstColumnIndex(), extra.getLastRowIndex(),
                        extra.getLastColumnIndex());
                break;
            default:
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        logger.info("EXCEL数据解析完毕!");
    }


    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        this.headMap = headMap;
        logger.debug("解析到一条表头数据: {}", new JSONObject(headMap));
    }

}
