package com.wrop.excel.helper.spring.configure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "excel-helper")
public class ExcelHelperProperties {

    /**
     * 多选情况下的分隔符
     */
    private String multiSelectSeparator = ",";

    public String getMultiSelectSeparator() {
        return multiSelectSeparator;
    }

    public void setMultiSelectSeparator(String multiSelectSeparator) {
        this.multiSelectSeparator = multiSelectSeparator;
    }
}
