package com.wrop.excel.helper.spring.configure;

import com.wrop.excel.helper.annotation.ExcelModel;
import com.wrop.excel.helper.spring.ExcelModelRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.StringUtils;

import java.util.List;

public class ModelScanConfiguration implements BeanFactoryAware, ImportBeanDefinitionRegistrar {

    private static final Logger logger = LoggerFactory.getLogger(ModelScanConfiguration.class);

    private BeanFactory beanFactory;

    private final ExcelModelRegistry excelModelRegistry = new ExcelModelRegistry();

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

        if (!AutoConfigurationPackages.has(this.beanFactory)) {
            logger.debug("Could not determine auto-configuration package, automatic excel model scanning disabled.");
            return;
        }
        logger.debug("Searching for excel annotated with @ExcelModel");
        List<String> packages = AutoConfigurationPackages.get(this.beanFactory);
        if (logger.isDebugEnabled()) {
            packages.forEach(pkg -> logger.debug("Using auto-configuration base package '{}'", pkg));
        }

        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(ExcelModelRegistry.class);
        registry.registerBeanDefinition(ExcelModelRegistry.class.getName(), beanDefinitionBuilder.getBeanDefinition());
        ((ConfigurableListableBeanFactory)beanFactory).registerSingleton(ExcelModelRegistry.class.getName(), excelModelRegistry);


        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(ExcelModelScannerConfigurer.class);
        builder.addPropertyValue("annotationClass", ExcelModel.class);
        builder.addPropertyValue("basePackage", StringUtils.collectionToCommaDelimitedString(packages));
        builder.addPropertyValue("excelModelRegistry", excelModelRegistry);
        registry.registerBeanDefinition(ExcelModelScannerConfigurer.class.getName(), builder.getBeanDefinition());
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }
}
