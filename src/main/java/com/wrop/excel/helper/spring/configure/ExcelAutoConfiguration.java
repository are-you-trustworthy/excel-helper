package com.wrop.excel.helper.spring.configure;

import com.wrop.excel.helper.core.analysis.*;
import com.wrop.excel.helper.core.analysis.impl.DefaultRequiredFieldFilter;
import com.wrop.excel.helper.core.analysis.impl.DefaultRequiredFieldsCollector;
import com.wrop.excel.helper.core.analysis.impl.LocalExcelCache;
import com.wrop.excel.helper.core.validate.ExcelRowValidator;
import com.wrop.excel.helper.core.write.DropDownSelectHandler;
import com.wrop.excel.helper.core.write.MultiDropDownSelectStrategy;
import com.wrop.excel.helper.core.write.RequiredFieldHeadHandler;
import com.wrop.excel.helper.core.write.SingleDropDownSelectStrategy;
import com.wrop.excel.helper.core.write.impl.DefaultCellWriteHandler;
import com.wrop.excel.helper.core.write.impl.DefaultMultiSelectStrategy;
import com.wrop.excel.helper.core.write.impl.DefaultSingleSelectStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.web.context.WebApplicationContext;

@Configuration
@EnableConfigurationProperties(ExcelHelperProperties.class)
@Import(ModelScanConfiguration.class)
public class ExcelAutoConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(ExcelAutoConfiguration.class);

    @Bean
    public DefaultCellWriteHandler defaultCellWriteHandler(DropDownSelectHandler dropDownSelectHandler,
                                                           RequiredFieldHeadHandler requiredFieldHeadHandler) {
        DefaultCellWriteHandler defaultCellWriteHandler = new DefaultCellWriteHandler();
        defaultCellWriteHandler.setDropDownSelectHandler(dropDownSelectHandler);
        defaultCellWriteHandler.setRequiredFieldHeadHandler(requiredFieldHeadHandler);
        return defaultCellWriteHandler;
    }

    @Bean
    @ConditionalOnMissingBean(DropDownSelectHandler.class)
    public DropDownSelectHandler dropDownSelectHandler(MultiDropDownSelectStrategy multiDropDownSelectStrategy,
                                                       SingleDropDownSelectStrategy singleDropDownSelectStrategy) {

        return new DropDownSelectHandler(multiDropDownSelectStrategy, singleDropDownSelectStrategy);
    }

    @Bean
    public RequiredFieldAnalyser requiredFieldAnalyser(RequiredFieldFilterChain requiredFieldFilterChain,
                                                       @Autowired(required = false) RequiredFieldsCollector requiredFieldsCollector) {
        RequiredFieldAnalyser requiredFieldAnalyser = new RequiredFieldAnalyser();
        requiredFieldAnalyser.setFilterChain(requiredFieldFilterChain);
        requiredFieldAnalyser.setRequiredFieldsCollector(requiredFieldsCollector);
        return requiredFieldAnalyser;
    }

    @Bean
    @ConditionalOnMissingBean(RequiredFieldHeadHandler.class)
    public RequiredFieldHeadHandler requiredFieldHeadHandler(RequiredFieldAnalyser requiredFieldAnalyser) {
        return new RequiredFieldHeadHandler(requiredFieldAnalyser);
    }

    @Bean
    @ConditionalOnMissingBean(ExcelRequiredFieldsCache.class)
    @ConditionalOnProperty(value = "excel-helper.cache.enabled", havingValue = "true", matchIfMissing = true)
    public ExcelRequiredFieldsCache requiredFieldsCache(RequiredFieldAnalyser requiredFieldAnalyser, DropDownSelectHandler dropDownSelectHandler) {
        LocalExcelCache localExcelCache = new LocalExcelCache();
        requiredFieldAnalyser.setCacheEnabled(true);
        requiredFieldAnalyser.setRequiredFieldsCache(localExcelCache);

        dropDownSelectHandler.setCacheEnabled(true);
        dropDownSelectHandler.setExcelSelectOptionCache(localExcelCache);
        return localExcelCache;
    }

    @Bean
    @ConditionalOnMissingBean(MultiDropDownSelectStrategy.class)
    public MultiDropDownSelectStrategy multiDropDownSelectStrategy() {
        return new DefaultMultiSelectStrategy();
    }

    @Bean
    @ConditionalOnMissingBean(SingleDropDownSelectStrategy.class)
    public SingleDropDownSelectStrategy singleDropDownSelectStrategy() {
        return new DefaultSingleSelectStrategy();
    }

    @Configuration
    static class RequiredFieldFilterConfiguration {

        @Bean
        public RequiredFieldFilter defaultRequiredFieldFilter() {
            return new DefaultRequiredFieldFilter();
        }

        @Bean
        public RequiredFieldFilterChain requiredFieldFilterChain(RequiredFieldFilter[] validators) {
            RequiredFieldFilterChain validatorChain = new RequiredFieldFilterChain();
            validatorChain.addValidator(validators);
            return validatorChain;
        }

        @Bean
        @ConditionalOnMissingBean(ExcelRowValidator.class)
        public ExcelRowValidator excelRowValidator() {
            return (row, args) -> {
                // do nothing
                return null;
            };
        }

        @Configuration
        @ConditionalOnWebApplication
        static class CollectorConfiguration {

            @Bean
            @ConditionalOnMissingBean
            @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
            public RequiredFieldsCollector defaultRequiredFieldsCollector() {
                return new DefaultRequiredFieldsCollector();
            }
        }
    }

}
