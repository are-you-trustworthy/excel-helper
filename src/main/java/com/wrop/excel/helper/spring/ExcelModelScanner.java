package com.wrop.excel.helper.spring;

import com.wrop.excel.helper.annotation.ExcelModel;
import com.wrop.excel.helper.core.ModelBean;
import com.wrop.excel.helper.dao.DataDao;
import com.wrop.excel.helper.util.ReflectUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Set;

/**
 * 扫描ExcelModel
 */
public class ExcelModelScanner extends ClassPathBeanDefinitionScanner {

    private static final Logger logger = LoggerFactory.getLogger(ExcelModelScanner.class);

    private final ExcelModelRegistry excelModelRegistry;

    private Class<? extends Annotation> annotationClass;

    private String basePackage;

    public ExcelModelScanner(BeanDefinitionRegistry registry, ExcelModelRegistry excelModelRegistry) {
        super(registry, false);
        this.excelModelRegistry = excelModelRegistry;
    }

    public Class<? extends Annotation> getAnnotationClass() {
        return annotationClass;
    }

    public void setAnnotationClass(Class<? extends Annotation> annotationClass) {
        this.annotationClass = annotationClass;
    }

    public String getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }

    public void registerFilters() {

        if (this.annotationClass != null) {
            addIncludeFilter(new AnnotationTypeFilter(this.annotationClass));
        }

        // exclude package-info.java
        addExcludeFilter((metadataReader, metadataReaderFactory) -> {
            String className = metadataReader.getClassMetadata().getClassName();
            return className.endsWith("package-info");
        });
    }

    public ExcelModelRegistry getExcelModelRegistry() {
        return excelModelRegistry;
    }

    @Override
    public Set<BeanDefinitionHolder> doScan(String... basePackages) {
        Set<BeanDefinitionHolder> beanDefinitions = super.doScan(basePackages);

        if (beanDefinitions.isEmpty()) {
            logger.warn("No Excel Model was found in packages: [{}]", Arrays.toString(basePackages));
        } else {
            processBeanDefinitions(beanDefinitions);
        }

        return beanDefinitions;
    }

    private void processBeanDefinitions(Set<BeanDefinitionHolder> beanDefinitions) {
        GenericBeanDefinition definition;
        for (BeanDefinitionHolder holder : beanDefinitions) {
            definition = (GenericBeanDefinition) holder.getBeanDefinition();
            String beanClassName = definition.getBeanClassName();
            logger.debug("Creating ModelBean with name '{}'", beanClassName);
            ModelBean modelBean = doCreateModelBean(definition);
            registerModelBean(modelBean);
        }
    }

    private ModelBean doCreateModelBean(BeanDefinition definition) {
        String beanClassName = definition.getBeanClassName();
        Class<?> cl;
        try {
            cl = Class.forName(beanClassName);
        } catch (ClassNotFoundException e) {
            logger.error("Class {} Not Found", beanClassName);
            return null;
        }
        ModelBean modelBean = new ModelBean();
        ExcelModel annotation = cl.getAnnotation(ExcelModel.class);
        modelBean.setBeanClass(cl);
        modelBean.setBid(annotation.value());
        modelBean.setDescription(annotation.description());
        modelBean.setBeanName(beanClassName);
        modelBean.setValidateClass(annotation.validateClass());
        modelBean.setReadListener(annotation.readListener());
        modelBean.setDao(annotation.dao());
        Class<?> entityClass = ReflectUtil.getSuperClassGenericType(annotation.dao(), DataDao.class, 0);
        modelBean.setEntityClass(entityClass);
        return modelBean;
    }

    private void registerModelBean(ModelBean modelBean) {
        if (modelBean != null) {
            excelModelRegistry.register(modelBean);
        }
    }
}
