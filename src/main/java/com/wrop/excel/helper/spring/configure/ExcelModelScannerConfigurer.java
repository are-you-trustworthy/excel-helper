package com.wrop.excel.helper.spring.configure;

import com.wrop.excel.helper.spring.ExcelModelRegistry;
import com.wrop.excel.helper.spring.ExcelModelScanner;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

import java.lang.annotation.Annotation;

public class ExcelModelScannerConfigurer implements BeanDefinitionRegistryPostProcessor {

    private Class<? extends Annotation> annotationClass;

    private String basePackage;

    private ExcelModelRegistry excelModelRegistry;

    public ExcelModelScannerConfigurer() {
    }

    public ExcelModelRegistry getExcelModelRegistry() {
        return excelModelRegistry;
    }

    public void setExcelModelRegistry(ExcelModelRegistry excelModelRegistry) {
        this.excelModelRegistry = excelModelRegistry;
    }

    public Class<?> getAnnotationClass() {
        return annotationClass;
    }

    public void setAnnotationClass(Class<? extends Annotation> annotationClass) {
        this.annotationClass = annotationClass;
    }

    public String getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
        ExcelModelScanner scanner = new ExcelModelScanner(beanDefinitionRegistry, excelModelRegistry);
        scanner.setBasePackage(this.basePackage);
        scanner.setAnnotationClass(this.annotationClass);
        scanner.registerFilters();
        scanner.doScan(this.basePackage);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }
}
