package com.wrop.excel.helper.spring;

import com.wrop.excel.helper.core.ModelBean;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * ExcelModel注册器
 */
public class ExcelModelRegistry {

    private static final HashMap<String, ModelBean> EXCEL_MODEL_POOL = new HashMap<>(8);

    public ExcelModelRegistry() {}

    public void register(ModelBean modelBean) {
        String bid = modelBean.getBid();
        EXCEL_MODEL_POOL.put(bid, modelBean);
    }

    public void unregister(ModelBean modelBean) {
        String bid = modelBean.getBid();
        unregister(bid);
    }

    public void unregister(String key) {
        EXCEL_MODEL_POOL.remove(key);
    }

    public Map<String, ModelBean> getAllModelBean() {
        return Collections.unmodifiableMap(EXCEL_MODEL_POOL);
    }

    public boolean contains(String key) {
        return EXCEL_MODEL_POOL.containsKey(key);
    }

    public ModelBean getModelBean(String key) {
        return EXCEL_MODEL_POOL.get(key);
    }

    public int modelCount() {
        return EXCEL_MODEL_POOL.size();
    }
}
