package com.wrop.excel.helper.util;

import cn.hutool.core.util.ReflectUtil;

public class BeanUtil {

    public static Object copy(Object source, Class<?> targetClass, String... ignoreProperties) {
        Object target = ReflectUtil.newInstanceIfPossible(targetClass);
        if (target != null) {
            cn.hutool.core.bean.BeanUtil.copyProperties(source, target, ignoreProperties);
        }
        return target;
    }

}
