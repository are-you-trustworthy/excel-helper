package com.wrop.excel.helper.util;

import cn.hutool.extra.spring.SpringUtil;
import com.wrop.excel.helper.core.validate.ExcelRowValidator;
import com.wrop.excel.helper.dao.DataDao;

public class ExcelUtil {

    private static final ExcelRowValidator VALID = SpringUtil.getBean(ExcelRowValidator.class);

    public static <T> void validate(T object, Object... args) {
        VALID.validate(object, args);
    }

    public static DataDao<?> getDataDao(Class<? extends DataDao<?>> daoClass, Class<?> entityClass) {
        DataDao<?> bean = SpringUtil.getBean(daoClass);
        Class<?> superClassGenericType = ReflectUtil.getSuperClassGenericType(daoClass, DataDao.class, 0);
        if (superClassGenericType == entityClass) {
            return bean;
        } else {
            return null;
        }
    }
}
