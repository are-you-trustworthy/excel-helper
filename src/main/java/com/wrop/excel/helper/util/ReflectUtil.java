package com.wrop.excel.helper.util;

import org.springframework.core.GenericTypeResolver;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ReflectUtil {

    public static Class<?> getSuperClassGenericType(Class<?> cl, Class<?> genericIfc, int index) {
        Class<?>[] typeArguments = GenericTypeResolver.resolveTypeArguments(cl, genericIfc);
        return null == typeArguments ? null : typeArguments[index];
    }

    public static Type getFieldGenericType(Field field, int index) {
        Type genericType = field.getGenericType();
        if (genericType instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) genericType).getActualTypeArguments();
            return actualTypeArguments[index];
        } else {
            return null;
        }
    }
}
